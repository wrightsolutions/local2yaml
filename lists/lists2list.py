#!/usr/bin/env python
#
#  Copyright 2016 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from ast import literal_eval
import fileinput
import re
from string import printable

SET_PRINTABLE=set(printable)

RE_SQUARE_BRACKETED = re.compile("^\[.*\]".format(chr(91),chr(93)))
RE_SQUARE_BRACKETED_LOOSE = re.compile(r"\s*\[.*\]".format(chr(91),chr(93)))
# Regex above checks if appears within [ ]

TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))
#print(len(TRANSLATE_DELETE))

list_combined = []

def list_combined_append(linenum,line_to_append,verbosity=0,loose=False):

        global list_combined
        
        combine_rc = 9999

        line_stripped = line_to_append.strip()
        len_line_stripped = len(line_stripped)

        if len_line_stripped < 1:
                # empty line or just whitespace
                return 0
        elif len_line_stripped == 2:
                # Assume empty brackets
                if loose is True:
                        return 0
                else:
                        return 5000
        else:
                pass
        
        if RE_SQUARE_BRACKETED.match(line_to_append):
                line_as_list = literal_eval(line_stripped)
                list_combined.extend(line_as_list)
                combine_rc = 0
        elif loose is True and RE_SQUARE_BRACKETED_LOOSE.match(line_to_append):
                line_as_list = literal_eval(line_stripped)
                list_combined.extend(line_as_list)
                combine_rc = 0
        else:
                if verbosity > 0:
                        print("line %d return code of %d for line=%s" %
                              (linenum,combine_rc,line_to_append))
                combine_rc = 5500
                
        return combine_rc


def process_lines(lines,problem_count_max=0,verbosity=0,loose=False):
        """ Process the lines
        problem_count_max of zero (default) means 'no maximum' so never break out
        of loop because of controlled problem.
        """
        
        lines_with_problems = 0

        line_rc = 999
        
        for idx,line in enumerate(lines):

                #print("processing line %d" % idx)
                line_rc = 0
                
                if set(line).issubset(SET_PRINTABLE):
                        pass
                else:
                        line_rc = 110
                        lines_with_problems += 1
                        print('line %s has one or more unprintables!' % idx)
                        
                line_translated = line.translate(None,TRANSLATE_DELETE)
                if len(line) != len(line_translated):
                        line_rc = 120
                        lines_with_problems += 1
                        print('line %s has tab or other low range chr()' % idx)
                else:
                        cret = list_combined_append(idx,line,verbosity,loose)
                        
                        if cret > 0:
                                line_rc = 200
                                lines_with_problems += 1
                                print('line %s unbracketed or other problem (rc=%d) combining.' % (idx,cret))

                #print("processed line %d" % idx)

        if verbosity > 1:
                print("final line %d processed with line return code of %d" % (idx,line_rc))

        if len(list_combined) > 0:
                print(list_combined)
                
        return lines_with_problems


if __name__ == "__main__":
	#lret = process_lines(fileinput.input(inplace=False),0,1)
        lret = process_lines(fileinput.input(inplace=False),0,1,True)

